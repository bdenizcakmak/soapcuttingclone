using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public GameObject startPanel;

    public void StartButton()
    {
        startPanel.SetActive(false);
    }
}
