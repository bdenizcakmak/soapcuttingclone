using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeManager : MonoBehaviour
{
    public float speed;
    private Rigidbody rb;
    public GameObject target;
    public ParticleSystem particle;
    public SoapManager instanceSoap;
    private bool isLayerDone;
    private float step;
    private void Awake()        
    {
        rb = GetComponent<Rigidbody>();
    }                   

    private void Start()
    {
        instanceSoap = SoapManager.Instance;
        step =  speed * Time.deltaTime; // calculate distance to move
    }
    private void Update()
    {
        MoveKnife();

        if (isLayerDone) //if first layer complete, remove it
        {
            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, step);
        }
    }

    public void MoveKnife()
    {
        float horizontalInput = Input.GetAxis("Horizontal") * Time.deltaTime;  
        float verticalInput = Input.GetAxis("Vertical") * Time.deltaTime;
        
        Vector3 movementDirection = new Vector3(horizontalInput, 0, verticalInput);
        movementDirection.Normalize();     
        
        rb.velocity = new Vector3(horizontalInput*speed, rb.velocity.y, verticalInput*speed);
    }
    
    void OnCollisionEnter(Collision collision)
    {

        if (collision.collider.name == "Cube" || collision.collider.name == "Cube2") //set boundries for movement
        {
            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, step);
            //transorm to first position
        }
        else
        {
            MoveKnife();
        }

        if(collision.collider.name == "Layer1" && collision.collider.name == "Cube")//if it is collides to layer end cube same time
        //it is the end of layer, it should dissappear
        {
            //particle.gameObject.SetActive(true);   particle moves
            instanceSoap.soapLayers[0].SetActive(false);
            isLayerDone = true;
            //move to next layer
            transform.position = Vector3.MoveTowards(transform.position, transform.position - new Vector3(0,0.1f,0), step);
        } 
        else if(collision.collider.name == "Layer2" && collision.collider.name == "Cube")
        {
            instanceSoap.soapLayers[1].SetActive(false);        
            transform.position = Vector3.MoveTowards(transform.position, transform.position - new Vector3(0,0.1f,0), step);
        }
        else if(collision.collider.name == "Layer3" && collision.collider.name == "Cube")
        {
            instanceSoap.soapLayers[2].SetActive(false);
        }
    }
}
